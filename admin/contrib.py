from core import app
from flask_admin import Admin
from flask_admin.contrib.mongoengine import ModelView
from chat.models import Message, Channel

admin = Admin(app)
admin.add_view(ModelView(Message, 'Messages')) # Register message model in Admin
admin.add_view(ModelView(Channel, 'Channels')) # Register channel model in Admin