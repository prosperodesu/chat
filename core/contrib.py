from core import app
import logging
from logging import FileHandler
from mongoengine import connect
from core.settings import DEBUG, UPLOAD_FOLDER, STATIC_DIR, mongod_settings
from chat.routes import site_index, site_sockets

if DEBUG == True:
    file_handler = FileHandler("debug.log","a")
    file_handler.setLevel(logging.WARNING)
    app.logger.addHandler(file_handler)
    from werkzeug import SharedDataMiddleware
    import os
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
      '/': STATIC_DIR
    })

connect(**mongod_settings)

app.register_blueprint(site_index)
app.register_blueprint(site_sockets)
