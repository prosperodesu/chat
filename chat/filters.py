from core import app

@app.template_filter('data_filter')
def data_filter(s):
    return s.strftime('%H:%M:%S')
