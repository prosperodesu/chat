from core import app
from flask.blueprints import Blueprint
from flask.templating import render_template
from chat.forms import MessageForm, ChannelForm
from chat.models import Message, Channel
from datetime import datetime
from flask.ext.socketio import SocketIO

site_index = Blueprint('home', __name__, template_folder='templates')
site_sockets = Blueprint('echo', __name__)
socketio = SocketIO(app)

@app.route('/', methods=['GET', 'POST'])
def index():

    messages = Message.objects()
    channels = Channel.objects()
    form = MessageForm()
    channel_form = ChannelForm()

    if form.validate_on_submit():
        message = form.content.data # Get message
        new_message = Message(content=message, author='Anon', created=datetime.now()) # To database
        new_message.save() # Save to DB

    if channel_form.validate_on_submit():
        title = channel_form.title.data
        new_channel = Channel(title=title)
        new_channel.save()

    return render_template('index.html',
                           form=form,
                           channel_form=channel_form,
                           messages=messages,
                           channels=channels)

@sockets.route('/echo')
def echo_socket(ws):
    while True:
        message = ws.receive()
        ws.send(message)
