from mongoengine import Document
from mongoengine.fields import *

class Message(Document):
    content = StringField(max_length=500, required=True)
    created = DateTimeField()
    author = StringField()

class Channel(Document):
    title = StringField(max_length=25)
    subtitle = StringField(max_length=100)
    creator = StringField()
    created = DateTimeField