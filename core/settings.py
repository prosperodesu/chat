from core.contrib import app
from admin.contrib import admin
import os
from chat.filters import data_filter

autoreload = True # Autoreload after changes
port = 8000
threaded = True # Multithreading

app.secret_key = '@s@#sss234Sd1445151a'
CSRF_ENABLED = True

DEBUG = True

mongod_settings = {
    'db': 'webchat', # Name of the database(username, password, host, port)
}

STATIC_DIR = os.path.join(os.path.dirname(__file__), 'static')
UPLOAD_FOLDER = os.path.join(os.path.dirname(__file__), 'upload')

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

app.jinja_env.filters['data_filter'] = data_filter