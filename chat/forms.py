from flask.ext.wtf import Form
from wtforms.fields import *
from wtforms.validators import DataRequired

class MessageForm(Form):
    content = TextAreaField(validators=[DataRequired(message='Message is required!')])

class ChannelForm(Form):
    title = StringField(validators=[DataRequired(message='Title is required!')])
    password = PasswordField()